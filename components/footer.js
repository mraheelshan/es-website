import React, { useEffect, useState } from "react";
import { Row, Col, Space, Typography, Divider } from 'antd';
import FooterLogo from '../public/images/appLogo.jpeg';
import Image from 'next/image';
import { useLayout } from "../providers/layout-provider";
import { FacebookOutlined, InstagramOutlined, PhoneOutlined } from '@ant-design/icons'
import classes from '../styles/Header.module.css'
import Link from 'next/link';
import ajaxService from "../services/ajax-service";

const { Text, Title } = Typography

function Footer() {

    const [isMobileScreen] = useLayout();
    const [blogs, setBlogs] = useState([])


    useEffect(() => {

        const getCommonData = async () => {
            const response = await ajaxService.get(`blogs`);

            if (response.authorize && response.valid) {
                setBlogs(response.payload)
            }
        }

        getCommonData();



    }, []);
    return (
        <>
            <div style={isMobileScreen ? { padding: '10px', backgroundColor: '#000' } : { padding: '45px', backgroundColor: '#000' }} >
                <Divider />
                {isMobileScreen ?
                    <Row gutter={[16, 16]} >
                        <Col className='text-center' xs={24} sm={24} md={6} lg={6} xl={6}>
                            <Link href="/">
                                <Image src={FooterLogo} height={100} width={100} />
                            </Link>
                            <div >
                                <Space size='large'>
                                    <a className="text-dark" href="https://www.facebook.com/theadeelfragrance" target="_blank" rel="noreferrer" >
                                        <FacebookOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                    <a className="text-dark" href="https://www.instagram.com/adeeliqbal0017/" target="_blank" rel="noreferrer" >
                                        <InstagramOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                    <a className="text-dark" href="tel:+61470520945">
                                        <PhoneOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                </Space>
                            </div>
                        </Col>
                    </Row> :

                    <Row gutter={[16, 16]} >
                        <Col className='' xs={24} sm={24} md={6} lg={6} xl={6} style={{ paddingLeft: 62 }}>
                            <Link href="/">
                                <Image src={FooterLogo} alt='logo' height={150} width={150} />
                            </Link>
                            <div className={classes.iconantd} style={{ paddingLeft: 17 }}>
                                <Space size='large'>
                                    <a className="text-dark" href="https://www.facebook.com/theadeelfragrance" target="_blank" rel="noreferrer" >
                                        <FacebookOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                    <a className="text-dark" href="https://www.instagram.com/adeeliqbal0017/" target="_blank" rel="noreferrer" >
                                        <InstagramOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                    <a className="text-dark" href="tel:+61470520945">
                                        <PhoneOutlined style={{ color: '#d0b369' }} />
                                    </a>
                                </Space>
                            </div>
                        </Col>
                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <h5 style={{ color: '#d0b369' }}>CUSTOMER SERVICES</h5>
                            <ul style={{ textDecoration: 'none' }} >
                                <li className='bottom-link' style={{ color: '#d0b369' }}  >
                                    <Link href={"/"}>
                                        Home
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}>
                                    <Link href="/privacy-policy">
                                        Privacy Policy
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}>
                                    <Link href="/terms-and-conditions">
                                        Terms & Conditions
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}>
                                    <Link href="/return-policy">
                                        Return Policy
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}>
                                    <Link href="/shipping-policy">
                                        Shipping Policy
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}>
                                    <Link href="/about-us">
                                        About Us
                                    </Link>
                                </li>
                                <li className='bottom-link' style={{ color: '#d0b369' }}  >
                                    <Link href={"/contact"}>
                                        Contact Us
                                    </Link>
                                </li>
                            </ul>
                        </Col>
                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <h5 style={{ color: '#d0b369' }}>OUR MISSION</h5>
                            <p style={{ color: '#d0b369' }}>At Adeel Fragrance, we are passionate about the transformative power of fragrance. Our mission is to curate a captivating collection of perfumes that inspire and empower individuals to express their unique identity.</p>
                            <br />
                            <br />
                        </Col>
                        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                            <h5 style={{ color: '#d0b369' }}>CONTACT US</h5>
                            <Title style={{ color: '#d0b369', fontSize: 18 }}>Address</Title>
                            <Text style={{ color: '#d0b369' }}>Willoughby Street , Reservoir, Victoria 3073 Australia</Text>
                            <Title style={{ color: '#d0b369', fontSize: 18 }}>Email</Title>
                            <Text style={{ color: '#d0b369' }}><a href="mailto:support@adeelfragrance.com">support@adeelfragrance.com</a></Text>
                        </Col>

                    </Row>
                }
            </div>
            <Row className="text-center pt-3" style={{ backgroundColor: '#000' }}>
                <Col md={8} xs={24} > <p style={{ color: '#d0b369' }}><small>Copyright © {new Date().getFullYear()} Adeel Fragrance. All Rights Reserved</small></p></Col>
                <Col md={8} xs={24} > <p style={{ color: '#d0b369' }}>
                    <Text style={{ color: '#d0b369' }}>ABN: 81154515178</Text>
                </p></Col>

                <Col md={8} xs={24} > <p style={{ color: '#d0b369' }}><small>Design & Developed By <a href="https://www.ohadtech.com/"> OhadTech</a> </small></p></Col>
            </Row>
        </>
    )
}

export default Footer;
