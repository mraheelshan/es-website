import { useEffect, useState } from 'react';
import classes from '../styles/Header.module.css'
import { FacebookOutlined, WhatsAppOutlined, InstagramOutlined, DownOutlined, ShoppingCartOutlined, SearchOutlined, AlignLeftOutlined } from '@ant-design/icons'
import Link from 'next/link';
import { Dropdown, Popover, Input, Space, Divider, Alert, Menu, Button, List, notification, Badge } from 'antd';
import Image from 'next/image';
import Logo from '../public/images/appLogo.jpeg'
import Marquee from 'react-fast-marquee';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { removeToken } from '../redux/actions/authAction';

const { Search } = Input;
const { SubMenu } = Menu;

const renderCategories = (router, mainCategory, categories) => {

    const navigate = (slug) => {
        router.push({ pathname: '/products/' + slug });
    };

    if (categories.length > 0) {
        return categories.map((category) => {

            if (mainCategory === null) {
                if (category.categories != undefined && category.categories.length > 0) {
                    return (
                        <SubMenu key={'cats-' + category.id} title={category.name} onTitleClick={() => navigate(category.slug)} style={{ width: '300px', textDecoration: 'none' }}>
                            {renderCategories(router, category, category.categories)}
                        </SubMenu>
                    )
                } else {
                    return (
                        <Menu.Item key={'cats-' + category.id}>
                            <Link href={'/products/' + category.slug} style={{ textDecoration: 'none' }} >
                                {category.name}
                            </Link>
                        </Menu.Item>
                    )
                }
            } else {
                if (category.categories != undefined && category.categories.length > 0) {
                    return (
                        <SubMenu key={'cats-' + category.id} title={category.name} onTitleClick={() => navigate(category.slug)} style={{ width: '300px', textDecoration: 'none' }}>
                            {renderCategories(router, category, category.categories)}
                        </SubMenu>
                    )
                } else {
                    return (
                        <Menu.Item key={'cats-' + category.id}>
                            <Link href={'/products/' + mainCategory.slug + '/' + category.slug} style={{ textDecoration: 'none' }} >
                                {category.name}
                            </Link>
                        </Menu.Item>
                    )
                }
            }
        })
    }
}

const CategoriesMenu = ({ categories }) => {
    const router = useRouter();

    return (
        <Menu mode="vertical">
            {renderCategories(router, null, categories)}
        </Menu>
    );
}

const LoggedIn = () => {

    const dispatch = useDispatch();
    const router = useRouter();

    const logoutUser = () => {
        dispatch(removeToken());
        router.push('/')
    }

    return (
        <Menu mode="vertical">
            <Menu.Item>
                <Link href="/user">
                    ACCOUNT
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Button type="primary" block onClick={logoutUser} styles={{ backgroundColor : '#000' }} >
                    LOGOUT
                </Button>
            </Menu.Item>
        </Menu>
    )
}

{/* <div style={{ margin: 'auto' }}>
    <Image src={Logo} alt="NY Store" width="130" height="43" />
</div> */}


const LoggedOut = () => {
    return (
        <Menu mode="vertical">
            <Menu.Item>
                WELCOME
            </Menu.Item>
            <Menu.Item style={{ backgroundColor : '#FFF' }}>
                <Button type="primary" block style={{ backgroundColor : '#000' }} >
                    <Link href="/login" style={{ backgroundColor : '#FFF' }}>
                        LOGIN
                    </Link>
                </Button>
            </Menu.Item>
            <Menu.Item>
                <Link href="/login">
                    NEW CUSTOMER? SIGN UP
                </Link>
            </Menu.Item>
        </Menu>
    )
}

const AuthDropdown = () => {

    const auth = useSelector(state => state.auth);
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        if (auth.token !== null) {
            setIsAuth(true)
        } else {
            setIsAuth(false)
        }
    }, [auth.token]);

    const renderDropdown = () => {
        if (isAuth) {
            return (
                <Dropdown overlay={<LoggedIn />} placement="bottomCenter" arrow   >
                    <span className="auth-link" >
                        <Link href="/">MY ACCOUNT</Link>
                    </span>
                </Dropdown>
            );
        } else {
            return (
                <Dropdown overlay={<LoggedOut />} placement="bottomCenter" arrow   >
                    <span className="auth-link" >
                        <Link href="/">LOGIN / REGISTER</Link>
                    </span>
                </Dropdown>
            );
        }
    }

    return renderDropdown()
}


function Header({ handleCartBar, ticker }) {

    const router = useRouter();

    const text = <span>Title</span>;

    const cartItems = useSelector(state => state.cart);

    let totalItems = 0;

    cartItems.map(item => {
        totalItems += item.Quantity;
    })

    const handleKeyword = ({ target }) => {

        if (target.value.length > 0) {
            router.push({
                pathname: '/search/' + target.value,
            });
        } else {
            notification.open({
                message: 'Keyword Required',
                description:
                    'Enter keyword to search.',
                onClick: () => {
                    //console.log('Notification Clicked!');
                }
            })
        }
    }

    return (
        <>
            <header>
                <div className={classes.container}>
                    <div style={{ display: 'flex', justifyContent: 'space-between', margin: '0px 0px' }}>
                        <div >
                            <Alert style={{ backgroundColor: 'black' }}
                                banner
                                message={
                                    <Marquee className='text-color' pauseOnHover gradient={false}>
                                        {ticker}
                                    </Marquee>
                                }
                            />
                        </div>

                        <div className={classes.iconantd} style={{ display: 'flex', justifyContent: 'space-between', width: '25%', backgroundColor: '#000' }}>
                            <Space direction='horizontal' size='small'>
                                <Space size='small' className="top-social">
                                    <a href="https://www.facebook.com/theadeelfragrance" target="_blank" rel="noreferrer" >
                                        <FacebookOutlined style={{ color : '#d0b369' }} />
                                    </a>
                                    <a href="https://www.instagram.com/adeeliqbal0017/" target="_blank" rel="noreferrer" >
                                        <InstagramOutlined style={{ color : '#d0b369' }}  />
                                    </a>
                                    <a href="tel:+61470520945">
                                        <WhatsAppOutlined style={{ color : '#d0b369' }}  />
                                    </a>
                                </Space>

                                <span className='top-link text-color'>
                                    <Link href="/">
                                        Home
                                    </Link>
                                </span>

                                <span className='top-link text-color'>
                                    <Link href="/about-us" >
                                        About Us
                                    </Link>
                                </span>

                                <span className='top-link text-color'  >
                                    <Link href={"/contact"}>
                                        Contact Us
                                    </Link>
                                </span>

                            </Space>
                        </div>
                    </div>
                </div>
            </header>

            <div className='sticky-top' style={{ backgroundColor: '#000', zIndex : 1 }}>
                <div className='navbar' style={{ display: 'flex', justifyContent: 'space-between', margin: '0px 55px', backgroundColor: '#000' }}>
                    <div className='' style={{ marginLeft: 45, marginRight: 45 }}>
                        <Link href={"/"}>
                            <Image src={Logo} alt='logo' width={100} height={100} />
                        </Link>
                    </div>
                    <div style={{ margin: 'auto' }}>
                        <Input onPressEnter={handleKeyword} size="large" placeholder="search for products" style={{ width: '38em', borderRadius: '8px' }} prefix={<SearchOutlined />} />
                    </div>
                    <div className='' style={{ marginLeft: 45, marginRight: 45 }}>
                        <Space >
                            <AuthDropdown />
                            <Badge count={totalItems}>
                                <ShoppingCartOutlined onClick={() => handleCartBar()} className={classes.iconantd} style={{ color : '#d0b369' }} />
                            </Badge>
                        </Space>
                    </div>
                </div>
            </div>

            <Divider style={{ margin: '0px', padding: '0px' }} />

            <div style={{ margin: '0px 25px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', margin: '5px 55px' }}>
                    <div style={{ display: 'flex', width: '50%', justifyContent: 'space-between' }}>

                        <Menu mode="horizontal" style={{ fontWeight: '500' }}>
                            <Menu.Item key="1" ><Link href={'/'}>HOME</Link> </Menu.Item>
                            <Menu.Item key="5" ><Link href={'/search/all'}>CATALOGUE</Link> </Menu.Item>
                            {/* <Menu.Item key="4" ><Link href="/blogs">BLOGS</Link> </Menu.Item> */}
                            <Menu.Item key="2" ><Link href="/about-us">ABOUT US</Link> </Menu.Item>
                            <Menu.Item key="3" ><Link href="/contact">CONTACT US</Link> </Menu.Item>
                            {/* <Menu.Item key="3" >Contact Us</Menu.Item> */}
                        </Menu>
                    </div>
                    <div style={{ marginTop: '16px', marginBottom: '16px' }} >
                        <h6 style={{ margin: 'auto' }} > <a href="tel:+61470520945" style={{ textDecoration: 'none', color : '#000' }}>CALL US (+61 470 520 945) </a> </h6>
                    </div>
                </div>
            </div>

            <Divider style={{ margin: '0px', padding: '0px' }} />
        </>
    )
}

export default Header;