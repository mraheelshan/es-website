import Image from 'next/image'
import classes from '../styles/App.module.css'
import CartBtn from '../public/images/btn-cart.svg'
import { Card, Typography } from 'antd';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { addToCart } from '../redux/actions/cartAction'
import { ShoppingCartOutlined } from '@ant-design/icons';
import { currency } from '../services/constants';
import { useLayout } from '../providers/layout-provider';
import { useRouter } from 'next/router';

const { Text } = Typography;

export default function CardComponent({ product }) {

    const [isMobileScreen] = useLayout();
    const router = useRouter();

    const dispatch = useDispatch();
    let { name, image, selling_price, slug, discount, offer, original_price, has_variant } = product

    const renderOffer = () => {
        if (offer) {
            return (
                <div>
                    <span style={{ color: '#000' }}><del>{currency} {selling_price}</del></span>
                    <span className={classes.percentofflabel}>-{discount}%</span>
                </div>
            );
        }
    }
    const renderOrignalPrice = () => {
        if (offer) {
            return original_price

        } else {
            return selling_price
        }
    }
    const handleAddToCart = () => {
        if(!has_variant){
            let productcart = { ...product }
            productcart.Quantity = 1;
            productcart.selling_price = renderOrignalPrice()
            dispatch(addToCart(productcart))
        }else{
            router.push({ pathname: '/product/' + slug });
        }
    }

    return (
        <Card hoverable style={{ background: '#fff', padding: '5px', borderRadius: '7px', height: '100%' }}>
            <Link href={'/product/' + slug}>
                <Image src={image} width={'100%'} height={'100%'} layout="responsive" objectFit="contain" />
            </Link>
            <div className='px-2'>
                <Link href={'/product/' + slug}>
                    <Text ellipsis={true} title={name} style={{ fontSize: isMobileScreen ? 16 : 21, marginTop: 10 }} >{name}</Text>
                </Link>
            </div>
            <div className='p-2' style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'baseline' }} >
                <div>
                    <strong style={{ color: ' #000', fontSize: isMobileScreen ? 12 : 17 }} >{currency} {renderOrignalPrice()}</strong>
                    {renderOffer()}

                </div>
                <div onClick={() => handleAddToCart()} className={classes.carticon}>
                    <ShoppingCartOutlined style={{ fontSize: isMobileScreen ? 20 : 25 }} />
                </div>
            </div>
        </Card>
    )
}
