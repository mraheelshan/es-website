import React, { useEffect, useState } from 'react';
import { Carousel } from 'antd';
import Image from 'next/image';


function CarouselSlid({ banners }) {
    const aspectRatio = 96 / 35; // Your desired aspect ratio (width / height)
    const [width, setWidth] = useState(0)
    const [imageHeight, setImageHeight] = useState(0);


    useEffect(() => {
        const updateImageHeight = () => {
            const screenWidth = document.body.clientWidth;
            const calculatedHeight = screenWidth / aspectRatio;
            setWidth(screenWidth)
            setImageHeight(calculatedHeight);
        };

        updateImageHeight();

    }, []);

    return (
        <Carousel autoplay>
            {
                banners.map((banner, index) => {
                    return (
                        <div key={'banner-' + index} style={{ width: '100%', height: '0', paddingBottom: '100%', position: 'relative' }}>
                            <Image src={banner.url}
                                width={width} height={imageHeight} layout="responsive" objectFit="contain"
                            />
                        </div>
                    );
                })
            }

        </Carousel>
    )
}

export default CarouselSlid;