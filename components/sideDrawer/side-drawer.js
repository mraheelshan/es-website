import classes from './drawer.module.css'
import { SearchOutlined, RightOutlined } from '@ant-design/icons'
import React, { useState, useEffect } from 'react';
import { Input, Tabs, Menu, Button, Typography } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { removeToken } from '../../redux/actions/authAction';

const { SubMenu } = Menu;
const { TabPane } = Tabs;
const { Text } = Typography;

export default function SideDrawer({ handleSideBar }) {
    const auth = useSelector(state => state.auth);
    const [isAuth, setIsAuth] = useState(false);

    const dispatch = useDispatch();
    const router = useRouter();

    function callback(key) {
        //   console.log(key);
    }
    const navigate = (slug) => {
        router.push({ pathname: '/products/' + slug });
        handleSideBar(false);
    };
    const handleCategory = (slug) => {
        navigate(slug);
        handleSideBar(false);
    }
    const handleKeyword = ({ target }) => {

        if (target.value.length > 0) {
            router.push({
                pathname: '/search/' + target.value,
            });
            handleSideBar(false);
        } else {
            notification.open({
                message: 'Keyword Required',
                description:
                    'Enter keyword to search.',
                onClick: () => {
                    //console.log('Notification Clicked!');
                }
            })
        }
    }
    const renderCategories = (categories) => {

        if (categories.length > 0) {
            return categories.map((category) => {
                if (category.categories != undefined && category.categories.length > 0) {
                    return (
                        <SubMenu key={'cats-' + category.id} title={<Text onClick={() => handleCategory(category.slug)}>{category.name}</Text>} style={{ background: '#fff' }}>
                            <Menu.ItemGroup style={{ background: '#fff' }} key={'sub-' + category.id}>
                                {renderCategories(category.categories)}
                            </Menu.ItemGroup>
                        </SubMenu>
                    )
                } else {
                    return (
                        <Menu.Item key={'cats-' + category.id} onClick={() => navigate(category.slug)} >
                            <Text onClick={() => handleSideBar()} > {category.name}</Text>
                        </Menu.Item>
                    )
                }
            })
        }
    }
    const logoutUser = () => {
        dispatch(removeToken());
        router.push('/')
    }

    const navigateToRoute = (route) => {
        handleSideBar(false);
        router.push(route)
    }
    useEffect(() => {
        if (auth.token !== null) {
            setIsAuth(true)
        } else {
            setIsAuth(false)
        }
    }, [auth.token]);
    return (

        <div className={classes.drawer}>
            <Input onPressEnter={handleKeyword} size="large" placeholder="search for products..." style={{ width: '100%' }} prefix={<SearchOutlined />} />
            <Menu>
                <Menu.Item key="m1" onClick={() => navigateToRoute('/')}  >Home</Menu.Item>
                <Menu.Item key="m7" onClick={() => navigateToRoute('/search/all')}  >Catalogue</Menu.Item>
                <Menu.Item key="m2" onClick={() => navigateToRoute('/about')}  >About Us</Menu.Item>
                {isAuth ? <Menu.Item key="m3" onClick={() => navigateToRoute('/user')} >My Account</Menu.Item> : <Menu.Item key="m3" onClick={() => navigateToRoute('/login')} >Login/Register</Menu.Item>}
                <Menu.Item key="m4" onClick={() => navigateToRoute('/privacy-policy')}  >Privacy Policy</Menu.Item>
                <Menu.Item key="m4" onClick={() => navigateToRoute('/return-policy')}  >Return Policy</Menu.Item>
                <Menu.Item key="m4" onClick={() => navigateToRoute('/shipping-policy')}  >Shipping Policy</Menu.Item>
                <Menu.Item key="m5" onClick={() => navigateToRoute('/terms-and-conditions')} >Term & Condition</Menu.Item>
                <Menu.Item key="m6" onClick={() => navigateToRoute('/contact')} >Contact Us</Menu.Item>
                {isAuth && <Menu.Item key="m6">
                    <Button type="primary" block danger onClick={() => logoutUser()}>
                        LOGOUT
                    </Button>
                </Menu.Item>}
                <Menu.Item key="m7"  >
                <Text style={{ color : '#000' }}>ABN: 81154515178</Text>
                </Menu.Item>
            </Menu>
        </div >
    )
}