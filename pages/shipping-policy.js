import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';

const { Title, Text } = Typography;

function ShippingPolicy() {
    return (
        <main>
            <Head>
                <title>Shipping Policy</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 bg-white rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Shipping Policy</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='bg-white mt-3 p-3' >
                    <Title>Shipping Policy</Title>
                    <Text>
                        Although packing is done under strict supervision, we are not responsible for any breakage, loss, or theft while in transit. Our Liability ceases after handling the parcel to the courier.
                        All deliveries are scheduled through reputed couriers. You will be informed when the seller ships your consignment and will also be provided a tracking number. We generally ship through Australia post wherever possible or will use other reputed courier service providers.
                    </Text>
                    <br /><br />
                    <Text>
                        The prices, Specifications and availability of items are subject to change without any prior notice.
                        The usual lead time is 5- 10 days(Domestic shipping) and 1-3 weeks (International shipping) from the date of receipt of the orders.
                        Those items for shipment to countries outside India may be subject to taxes, customs duties and fees levied by the destination country ("Import Fees"). The recipient of the shipment is the importer of record in the destination country and is responsible for all Import Fees.
                        Under no circumstances whatsoever shall Adeel fragrance be liable for any loss or damages whatsoever including, without limiting, any indirect, special, incidental, consequential, or other damages that result from the use of or inability to use the products/services offered on the site.
                    </Text>
                    <br /><br />
                    <Title level={4}>How do I cancel an order on Adeel Fragrance website?</Title>
                    <Text>
                        If unfortunately, you have to cancel an order, please do so at the earliest by emailing us at adeel173@hotmail.com
                    </Text>
                    <br /><br />
                    <Text>
                        For outright cancellations by you:
                    </Text>
                    <br /><br />
                    <Text>
                        -     If you cancel your order before your order has been shipped, we will arrange to refund the full amount back to the original mode of payment.
                    </Text>
                    <br /><br />
                    <Text>
                        If the cancellation is after your product has been shipped:
                    </Text>
                    <br /><br />
                    <Text>
                        -    There will be no refund.
                    </Text>
                </div>
            </div>
        </main>
    )
}
export default ShippingPolicy