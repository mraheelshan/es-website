import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';

const { Title, Text } = Typography;

function ReturnPolicy() {
    return (
        <main>
            <Head>
                <title>Return Policy</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 bg-white rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Return Policy</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='bg-white mt-3 p-3' >
                    <Title>Return Policy</Title>
                    <Title level={2}>RETURN CONDITIONS WARNING</Title>
                    <Text>
                        If seal is open return won't be accepted.
                    </Text>
                    <br /><br />
                    <Text>
                        Received defective / wrong product, while returning need a proof by creating video.
                    </Text>
                    <br /><br />
                    <Text>
                        Don't accept the parcel if seal is already open while receiving parcel.
                    </Text>
                    <br /><br />
                    <Text>
                        Free returns are available for all sale items within 7 days of receipt, provided the return conditions specified below are met.
                    </Text>
                    <br /><br />
                    <Text>
                        Return Instructions are provided below.
                    </Text>
                    <br /><br />
                    <Text>
                        Returns will be accepted only if there is any manufacturing defect with the product or mismatch of the product
                        Items returned must be in their unused original condition with all Adeel Fragrance item tags attached and any related sampling pieces and booklets included.
                    </Text>

                    <br /><br />
                    <Text>
                        Incomplete, damaged, or altered returns, or anything we at Adeel fragrance reasonably believe has been used, will not be accepted and therefore sent back to the customer.
                    </Text>
                    <br /><br />
                    <Text>
                        Return charges are not applicable if you met the above conditions
                    </Text>
                    <br /><br />


                    <Text>

                    </Text>
                </div>
            </div>
        </main>
    )
}
export default ReturnPolicy