import classes from '../styles/Checkout.module.css'
import { Breadcrumb, Button, Col, Row, Space, Typography } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import { removeAllCartItems } from '../redux/actions/cartAction'

import Head from 'next/head'
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

const { Text } = Typography

function Thankyou({ order_number }) {

    const dispatch = useDispatch();

    const router = useRouter();

    const redirectToHome = () => {
        router.push({ pathname: '/' })
    }

    const discardCart = () => {
        dispatch(removeAllCartItems());
    }

    useEffect(() => {
        discardCart();
    }, [])

    return (
        <main>
            <Head>
                <title>Checkout</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 bg-white rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item href="">Order</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='bg-white mt-3 p-3' >
                    <Row gutter={[32, 32]}>
                        <Col xs={0} md={8}></Col>
                        <Col className={classes.name} xs={24} md={8}>
                            <div style={{ background: '#fff', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '250px' }} >
                                <CheckCircleOutlined style={{ fontSize: 128 }} />
                                <Text style={{ marginTop: 20 }}>Your order has been placed</Text>
                                <Text style={{ marginTop: 20 , fontWeight : '700' }}>Order Number #{order_number}</Text>
                                <Space size={'middle'} style={{ marginTop : 20 }}>
                                    {/* <Button type="primary" danger onClick={redirectToHome}>View Order</Button> */}
                                    <Button type="primary" danger onClick={redirectToHome}>Start Over</Button>
                                </Space>
                            </div>
                        </Col>
                        <Col xs={0} md={8}></Col>
                    </Row>
                </div>
            </div>
        </main>
    )
}
export default Thankyou


Thankyou.getInitialProps = ({ query: { order_number } }) => {
    return { order_number }
}