import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';

const { Title, Text  } = Typography;

function ReturnPolicy() {

    return (
        <main>
            <Head>
                <title>Privacy Policy</title>
            </Head>
            <div className='py-3'>
            <Row className='p-3 border rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Privacy Policy</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='bg-white mt-3 p-3' >
                    <Title>Privacy Policy</Title>
                    <Text>
                        Adeel Fragrance is domain registered and sole trader business (who we refer to as "we", "us" and "our" below), take your privacy very seriously. This Privacy Statement explains what personal information we collect, how and when it is collected, what we use it for now and how we will use it in the FUTURE and details of the circumstances in which we may disclose it to third parties. If you have any questions about the way in which your information is being collected or used which are not answered by this Privacy Statement and/or any complaints please contact us on adeel173@hotmail.com
                    </Text>
                    <br /><br />
                    <Text>
                        By visiting and using www.adeel fragrance.com or any other application or website ("Application") for the purchase or sampling of products from Adeel fragrance (as applicable) you acknowledge that you have read, understood and are consenting to the practices in relation to use and disclosure of your personal information described in this Privacy Statement and our Terms and Conditions. Please obtain your parent's or guardian's consent before providing us with any personal information if you are visiting and using the Site; Anywhere else in the world and you are under the age of 16.
                    </Text>
                    <br /><br />
                    <Title level={4}>WHAT INFORMATION DO WE COLLECT AND HOW WILL WE USE IT?</Title>
                    <Text>
                        When you REGISTER for an account with us, place an order with us or send us an enquiry, we will collect certain personal information from you, for example, your name, postal address, phone numbers, e-mail addresses. We may also obtain information about you as a result of authentication or identity checks. We use this information to identify you as a customer, to process your order, to deliver products, to process payments, to update our records, to enable your use of interactive features of our service and to generally manage your account with us and if you have consented to provide you with information by post, e-mail, mobile messaging, telephone communications and/or through any other electronic means including social network platforms about our products, events, promotions and services. We may also use this information to tailor how our website appears to you and to tailor the contents of our communications with you, so as to make the website and those communications more relevant to you.
                    </Text>
                    <br /><br />
                    <Text>
                        We may also at times ask you for other information, for example product and category preferences, age and any special dates (such as birthday and anniversary) which will be used to enhance our service to you. Providing us with this sort of information is entirely voluntary, however we may not be able to process your order if you do not provide us with all the requested information.
                    </Text>
                    <br /><br />
                    <Text>
                        We may also use your personal information for our internal MARKETING and demographic studies, together with non-personal data to analyze, profile and monitor customer patterns so we can consistently improve our products and services and understand what may be of interest to you and other customers.                    </Text>
                    <br /><br />

                    <Title level={4}>RETENTION AND DELETION OF PERSONAL INFORMATION</Title>
                    <Text>
                        We shall only keep your information as long as you remain an active customer.
                    </Text>
                    <br /><br />
                    <Text>
                        If you wish to request the deletion of your personal details, or wish to change your preferences at any time please contact our Customer Service Team.
                    </Text>
                    <br /><br />
                    <Title level={4}>WHAT YOU CAN EXPECT FROM Adeel  Fragrance</Title>
                    <Text>
                        We will at all times seek to comply with the requirements of the Act to ensure that the personal information you give us is kept appropriately secure and processed fairly and lawfully.
                    </Text>
                    <br /><br />
                    <Title level={4}>OTHER PEOPLE WHO MIGHT USE YOUR INFORMATION</Title>
                    <Text>
                        “All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties”.
                    </Text>
                    <br /><br />
                    <Text>
                        You also acknowledge and agree that in certain circumstances we may disclose personal information relating to you to third parties, for example, in order to conform to any requirements of law, to comply with any legal process, for the purposes of obtaining legal advice, for the purposes of CREDIT RISK reduction, to prevent and detect fraud and/or to protect and defend the rights and property of Adeel Fragrance. At all times where we disclose your information for the purposes of credit risk reduction and fraud prevention we will take all steps reasonably necessary to ensure that it remains secure.
                    </Text>
                    <br /><br />

                    <Title level={4}> UPDATING AND REVIEWING YOUR PERSONAL DETAILS</Title>
                    <Text>
                        You can amend or update your information by logging into My ACCOUNT on the Website and amending your details as appropriate.
                    </Text>
                    <br /><br />

                    <Title level={4}>CHANGES TO THIS PRIVACY STATEMENT</Title>
                    <Text>

                        We may update this Privacy Statement from time to time. The amended Privacy Statement will be posted on the Website. Please check this page regularly for changes to this Privacy Statement. If you continue to use the Website or if you submit information to us following such changes, you will be deemed to have read and agreed them.

                    </Text>
                    <br /><br />

                    <Title level={4}>THIRD PARTY LINKS</Title>
                    <Text>


                        The Website  may from time to time contain links to other websites not controlled by us. We do not accept responsibility or LIABILITY for the privacy practices, data collected or the content of such other websites. The operators of these linked websites are not under a duty to abide by this Privacy Statement. If there are terms and conditions, other privacy statements or policies appearing on those websites, you should also review them carefully as your use of those sites may be subject to them.
                    </Text>
                    <br /><br />
                    <Title level={4}>SECURITY STATEMENT</Title>
                    <Text>
                        Unfortunately transmission of information via the Internet is not completely secure. Although we do our best to protect your personal data we cannot guarantee the security of your data submitted to us and any transmission is at your service.

                    </Text>
                </div>
            </div>
        </main >
    )
}
export default ReturnPolicy