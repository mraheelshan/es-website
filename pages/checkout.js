import React, { useState } from 'react';
import classes from '../styles/Checkout.module.css'
import { Breadcrumb, Button, Col, Divider, Row, Input, Space, Select, Radio, Table, notification, Form } from 'antd';
import Image from 'next/image';
import { InfoCircleOutlined, MinusOutlined, PlusOutlined } from '@ant-design/icons'
import { useSelector, useDispatch } from 'react-redux'
import { addToCart, subtractQuantity, removeAllCartItems } from '../redux/actions/cartAction'
import { cityData } from '../components/cityData';
import Head from 'next/head'
import { useEffect } from 'react'
import { useRouter } from 'next/router';
import ajaxService from '../services/ajax-service'
import { currency } from '../services/constants';
const { Option } = Select;
const { TextArea } = Input
import CreditCardInput from 'react-credit-card-input';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';

function haversineDistance(lat1, lon1, lat2, lon2) {
    // Radius of the Earth in kilometers
    const R = 6371;

    // Convert latitude and longitude from degrees to radians
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);

    // Haversine formula
    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * (Math.PI / 180)) * Math.cos(lat2 * (Math.PI / 180)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    // Calculate the distance
    const distance = R * c;

    return distance;
}


function Checkout({ data, google }) {
    const cartItems = useSelector(state => state.cart)
    const dispatch = useDispatch();
    const router = useRouter();
    const auth = useSelector(state => state.auth);
    let { user } = auth;
    const [form] = Form.useForm();
    const [shipping, setShipping] = useState(0);
    const [cardNumber, setCardNumber] = useState();
    const [cvc, setCvc] = useState();
    const [expiry, setExpiry] = useState();

    const [minimumOrder, setMinimumOrder] = useState(0)
    const [minimumDistance, setMinimumDistance] = useState(0)
    const [latitude, setLatitude] = useState(-37.7220057)
    const [longitude, setLongitude] = useState(144.9729001)
    const [chargesWithin, setChargesWithin] = useState(0)
    const [chargesOutof, setChargesOutof] = useState(0)
    const [mapClicked, setMapClicked] = useState(false)
    const [storeLatitude, setStoreLatitude] = useState(-37.7220057)
    const [storeLongitude, setStoreLongitude] = useState(144.9729001)

    const [total, setTotal] = useState(0)
    const [subtotal, setSubtotal] = useState(0)

    let quantity = 0;

    cartItems.map(item => {
        quantity += item.Quantity;
    });

    let initialValues = {
        name: '',
        email: '',
        phone: '',
        city: '',
        state: 'Victoria',
        address: '',
        zipcode: '',
        notes: '',
        shipping: 0,
        payment_method: 'Comm Bank',
        cart: [],

    };

    if (user !== null) {
        initialValues = { ...initialValues, ...user }
    }

    const onFinish = async (values) => {

        if (!mapClicked) {
            notification.error({
                message: `Error`,
                description: 'Map location is required. Please click on your location on map',
                placment: 'bottomCenter'
            })
            return false;
        }

        if (!cardNumber || !expiry || !cvc) {
            notification.error({
                message: `Error`,
                description: 'Invalid card information. Please fill all details',
                placment: 'bottomCenter'
            })
            return false;
        }

        values.shipping = shipping
        values.cart = [...cartItems];
        values.cardNumber = cardNumber;
        values.cvc = cvc;
        let parts = expiry.split('/')

        if (!(parts && parts.length > 0)) {
            return false;
        }
        values.month = parseInt(parts[0])
        values.year = parseInt(parts[1])

        values.shipping_address = `${values.address}, ${values.city}, ${values.state}, ${values.zipcode}`;
        delete values.address;
        delete values.state;

        values.latitude = latitude;
        values.longitude = longitude;

        let response = await ajaxService.post('checkout', values);

        if (response) {
            router.push({ pathname: '/thankyou', query: { order_number: response.order_number } })
        } else {
            notification.error({
                message: `Error`,
                description: 'Invalid card information. Please try again',
                placement: 'bottomCenter'
            })
        }
    };

    useEffect(() => {
        if (data.shipping !== undefined) {

            let { latitude, longitude, minimum_distance, minimum_order, charges_within, charges_outof } = data.shipping[0]

            setMinimumOrder(minimum_order)
            setMinimumDistance(minimum_distance)
            setStoreLatitude(latitude)
            setStoreLongitude(longitude)
            setChargesWithin(charges_within)
            setChargesOutof(charges_outof)

            setShipping(charges_within)

            let subtotal = 0;

            cartItems.map(item => {
                subtotal += item.Quantity * item.selling_price
            });

            setTotal(subtotal)
        }
    }, [])

    useEffect(() => {

        if (cartItems.length === 0) {
            router.push({ pathname: '/' })
        }

        let subtotal = 0;

        cartItems.map(item => {
            subtotal += item.Quantity * item.selling_price
        });

        setSubtotal(subtotal)
    }, [quantity])

    const discardCart = () => {
        dispatch(removeAllCartItems());
    }

    const handleCardNumberChange = (event) => {
        setCardNumber(event.target.value)
    }

    const handleCardExpiryChange = (event) => {
        setExpiry(event.target.value)
    }

    const handleCardCVCChange = (event) => {
        setCvc(event.target.value)
    }

    const onMapClicked = (mapProps, map, event) => {
        setLatitude(event.latLng.lat())
        setLongitude(event.latLng.lng())
        setMapClicked(true)
    }

    useEffect(() => {

        let distance = haversineDistance(latitude, longitude, storeLatitude, storeLongitude)

        if (distance <= minimumDistance) {
            setShipping(0)
        } else {
            setShipping(chargesWithin)
        }
    }, [latitude, longitude])

    useEffect(() => {
        if (subtotal >= minimumOrder) {
            setShipping(0)
        }else{
            setShipping(chargesWithin)
        }
    }, [subtotal])

    useEffect(() => {

        let total = 0;
        
        if(shipping > 0){

            if (subtotal >= minimumOrder) {
                total = parseFloat(subtotal)
            }else{
                total = parseFloat(subtotal) + parseFloat(shipping)
            }
        }else{
            total = parseFloat(subtotal)
        }

        setTotal(total)

    }, [shipping,subtotal])

    return (
        <main>
            <Head>
                <title>Checkout</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 border rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item href="">Checkout</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='mt-3' >
                    <Form
                        layout={'vertical'}
                        form={form}
                        name="control-hooks"
                        initialValues={initialValues}
                        onFinish={onFinish}
                    //onFinishFailed={onLoginFinishFailed}
                    >
                        <Row gutter={[32, 32]}>
                            <Col className={classes.name} xs={24} md={8}>
                                <div className='border p-3' style={{ borderRight: '1px solid lightGray', paddingRight: '15px', background: '#fff' }} >
                                    <h5>Billing Details</h5>
                                    <Divider />
                                    <Form.Item name="name" label="Name" rules={[
                                        {
                                            required: true,
                                            message: 'Name field is required',
                                        },
                                    ]}>
                                        <Input placeholder='Name' />
                                    </Form.Item>
                                    <Form.Item name="email" label="Email" rules={[
                                        {
                                            required: false,
                                            type: 'email',
                                            message: 'Email field is required',
                                        },
                                    ]}>
                                        <Input placeholder='Email' />
                                    </Form.Item>
                                    <Row gutter={16}>
                                        <Col span={24}>
                                            <Form.Item name="phone" label="Phone" rules={[
                                                {
                                                    required: true,
                                                    message: 'Phone field is required',
                                                },
                                            ]}>
                                                <Input placeholder='Mobile Number' />
                                            </Form.Item>
                                        </Col>
                                    </Row>

                                </div>

                                <div style={{ border: '1px solid lightGray', marginTop: 15, padding: '15px', borderRadius: '5px', background: '#fff' }} >
                                    <h5>Card Payment (Comm Bank)</h5>
                                    <hr />

                                    <CreditCardInput
                                        cardNumberInputProps={{ value: cardNumber, onChange: handleCardNumberChange }}
                                        cardExpiryInputProps={{ value: expiry, onChange: handleCardExpiryChange }}
                                        cardCVCInputProps={{ value: cvc, onChange: handleCardCVCChange }}
                                        fieldClassName="input"
                                    />

                                    <span style={{ display: 'flex', alignItems: 'center', marginTop: 10 }}>
                                        <InfoCircleOutlined style={{ marginRight: 10 }} />
                                        Card information is not saved
                                    </span>
                                </div>
                            </Col>

                            <Col xs={24} md={8}>

                                <div className='border p-3' style={{ borderRight: '1px solid lightGray', paddingRight: '15px', background: '#fff' }} >
                                    <h5>Shipping Address</h5>
                                    <Divider />
                                    <Form.Item name="address" label="Complete Address" rules={[
                                        {
                                            required: true,
                                            message: 'Complete Address field is required',
                                        },
                                    ]}>
                                        <Input placeholder='Address' />
                                    </Form.Item>

                                    <Form.Item name="state" label="State" rules={[
                                        {
                                            required: true,
                                            message: 'State is required',
                                        },
                                    ]}

                                        defaultValue={'Victoria'}
                                    >
                                        <Select
                                            allowClear
                                            placeholder="Select State"
                                            optionFilterProp="children"
                                        // onChange={(v) => handleShipping(v)}
                                        >
                                            <Option value={'New South Wales'}  > New South Wales</Option>
                                            <Option value={'Queensland'}  > Queensland</Option>
                                            <Option value={'South Australia'}  > South Australia</Option>
                                            <Option value={'Tasmania'}  > Tasmania</Option>
                                            <Option value={'Victoria'}  > Victoria</Option>
                                            <Option value={'Western Australia'}  > Western Australia</Option>
                                            <Option value={'Australian Capital Territory (ACT)'}  > Australian Capital Territory (ACT)</Option>
                                            <Option value={'Northern Territory'}  > Northern Territory</Option>
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="city" label="City" rules={[
                                        {
                                            required: true,
                                            message: 'City is required',
                                        },
                                    ]}>
                                        <Input placeholder='City' />
                                    </Form.Item>
                                    <Form.Item name="zipcode" label="Zipcode" rules={[
                                        {
                                            required: true,
                                            message: 'Zipcode is required',
                                        },
                                    ]}>
                                        <Input placeholder='Zipcode' />
                                    </Form.Item>
                                    <div style={{ height: 340, margin: 5, position: 'relative', overflow: 'hidden' }}>
                                        {storeLatitude > 0 && storeLongitude > 0 && <Map
                                            google={google}
                                            zoom={14}
                                            onClick={onMapClicked}
                                            mapTypeControl={false}
                                            center={{
                                                lat: latitude,
                                                lng: longitude
                                            }}
                                            initialCenter={
                                                {
                                                    lat: latitude,
                                                    lng: longitude
                                                }
                                            }
                                            centerAroundCurrentLocation={true}
                                            visible={true}
                                        >
                                            <Marker position={{ lat: latitude, lng: longitude }} />
                                        </Map>
                                        }
                                    </div>


                                </div>
                            </Col>

                            <Col xs={24} md={8}>
                                <div style={{ border: '1px solid lightGray', padding: '15px', borderRadius: '5px', background: '#fff' }} >
                                    <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '10px' }}>
                                        <h5>Order Summary</h5>
                                        <Button style={{ float: 'right' }} danger type='primary' onClick={() => discardCart()}>Discard Cart</Button>
                                    </div>
                                    <hr />
                                    {cartItems?.map((val) => {
                                        return (
                                            <>
                                                <Row gutter={16}>
                                                    <Col md={4} xs={4} >
                                                        <Image width='100' height='100' style={{ objectFit: 'cover' }} src={val.image} alt='pic' />
                                                    </Col>
                                                    <Col md={8} xs={12}>
                                                        <p>{val.name} <strong >{currency}:{val.selling_price}</strong></p>
                                                        <div className={classes.anticon} style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', border: '2px solid black', borderRadius: '20px', height: '32px', margin: 'auto' }}>
                                                            <p onClick={() => val.Quantity > 1 && dispatch(subtractQuantity(val))}><MinusOutlined /></p>
                                                            <p>{val.Quantity}</p>
                                                            <p onClick={() => { dispatch(addToCart(val)) }} ><PlusOutlined /></p>
                                                        </div>

                                                    </Col>
                                                    <Col md={8} xs={0}></Col>

                                                    <Col md={4} xs={8}  >
                                                        <strong style={{ float: 'right' }}>{currency}:{parseFloat(val.selling_price * val.Quantity).toFixed(2)}</strong>
                                                    </Col>
                                                </Row>
                                                < Divider />
                                            </>
                                        )
                                    })}

                                    {/* <Divider style={{ margin: '5px', display: 'none' }} /> */}

                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <strong>Subtotal</strong>
                                        <strong>{currency} {parseFloat(subtotal).toFixed(2)}</strong>
                                    </div>

                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <strong>Shipping Charges</strong>
                                        <strong>{currency} {parseFloat(shipping).toFixed(2)}</strong>
                                    </div>

                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <strong>Total</strong>
                                        <strong>{currency} {parseFloat(total).toFixed(2)}</strong>
                                    </div>

                                </div>

                                <div className='border p-3 mt-3' style={{ borderRight: '1px solid lightGray', paddingRight: '15px', background: '#fff' }} >
                                    <h5>Notes</h5>
                                    <Divider />
                                    <Form.Item name="notes" label="" >
                                        <TextArea rows={4} placeholder='Other instructions' />
                                    </Form.Item>
                                </div>

                                <Button type='primary' htmlType="submit" size='large' danger style={{ float: 'right', margin: '15px 0' }}>Place Order</Button>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        </main>
    )
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyDw5_uMY8aIzYDb0naGXFgCzLCx5IlGzpM'
})(Checkout);

export async function getStaticProps() {

    let data = {
        shipping: []
    };

    const response = await ajaxService.get(`home`);

    if (response !== undefined && response !== null) {
        data = response;
    }

    return { props: { data } }
}