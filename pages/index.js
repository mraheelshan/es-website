import React, { useEffect, useState } from 'react';
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import CarouselSlid from '../components/carosel'
import ProductContent from '../components/product-content';
import ajaxService from '../services/ajax-service'
import initPushNotifications from '../services/push-notifications';

function Home({ data }) {

  const [pageData, setPageData] = useState([]);
  const [isMobileScreen, setIsMobileScreen] = useState(false)

  useEffect(() => {
    const handleWidth = () => {
      const width = document.body.clientWidth
      return (width <= 768)
    }

    setIsMobileScreen(handleWidth())
  })

  useEffect(() => {
    setPageData(data.data);
    initPushNotifications();
  }, [])

  const renderProductSection = ({ title, products, index }) => {
    return (
      <div className={styles.mainContainer} key={'product-section-' + index + Math.random()} >
        <h1 style={{ textAlign: 'center' }}>{title}</h1>
        <ProductContent products={products} />
      </div>
    )
  }

  return (
    <>
      <Head>
        <title>Adeel Fragrance</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content='Health/Beauty · Supermarket/Convenience store' />
      </Head>
      <CarouselSlid banners={data.banners} style={styles.carouselStyles} />
      <div style={{ height: '100%', padding: '0px', paddingLeft: (isMobileScreen ? '10px' : '80px'), paddingRight: (isMobileScreen ? '10px' : '80px'), backgroundColor: '#fff' }}>
        <div className={styles.container}>
          {
            pageData.map((item, index) => {

              if (item.type === 1) {
                return renderProductSection(item, index);
              }
            })
          }
        </div>
      </div>
    </>
  )
}

export const getServerSideProps = async () => {

  let data = {
    banners: [],
    data: []
  };

  const response = await ajaxService.get(`home`);

  if (response !== undefined && response !== null) {
    data = response;
  }

  return { props: { data } }
}


Home.Layout = null;

export default Home;