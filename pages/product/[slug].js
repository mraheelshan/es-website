import { useEffect, useState, useRef } from 'react';
import { Breadcrumb, Button, Col, Divider, Row, Typography, List, Radio, Space, Carousel, notification } from 'antd';
import classes from '../../styles/ProductDetail.module.css'
import Image from 'next/image';
import { MinusOutlined, PlusOutlined } from '@ant-design/icons'
import { useLayout } from '../../providers/layout-provider'
import ajaxService from '../../services/ajax-service'
import Head from 'next/head'
import { useSelector, useDispatch } from 'react-redux'
import ProductContent from '../../components/product-content';
import { addToCart, setToCart } from '../../redux/actions/cartAction';
import parse from 'html-react-parser';
import { currency } from '../../services/constants';

const { Title, Text } = Typography;

function ProductDetail({ data }) {

    const [width, setWidth] = useState(0)
    const [variant, setvariant] = useState();
    const [quantity, setQuantity] = useState(1);
    const [isMobileScreen] = useLayout();
    const cartItems = useSelector(state => state.cart)
    const dispatch = useDispatch()
    const ref = useRef();

    const { product, variants, related_products } = data;
    let { category, discount, offer, selling_price, meta_title, name, meta_description,
        og_title,
        og_description,
        og_url,
        twitter_title,
        twitter_description,
        twitter_summary,
        twitter_url,
    } = product;

    if (category === undefined) {
        category = { name: 'N/A', slug: '' }
    }

    const goTo = (slide) => {
        ref.current.goTo(slide, false);
    };

    const handleAddToCart = () => {
        let item = cartItems.find(item => item.id === product.id);

        if (!item) {
            let productcart = { ...product }
            productcart.Quantity = quantity
            productcart.selling_price = renderOrignalPrice(item)
            dispatch(addToCart(productcart))
        } else {
            dispatch(setToCart(item))
        }
    }
    const handleVariant = () => {

        if (variant != undefined) {

            let item = cartItems.find(item => item.id === variant.id);

            console.log(variant);

            if (!item) {
                let productcart = { ...variant }
                productcart.Quantity = quantity
                dispatch(addToCart(productcart))
            } else {
                dispatch(setToCart(variant))
            }
        } else {
            notification.warning({
                message: 'Warning',
                description: 'Please select veriant',
                placement: 'bottom'
            });
        }
    }

    useEffect(() => {
        setWidth(document.body.clientWidth)
    }, [])

    const renderOffer = () => {
        if (offer) {
            return (
                <div>
                    <span className={classes.percentofflabel}><del>{currency} {selling_price}</del></span>
                    <span className={classes.percentofflabel}>-{discount}%</span>
                </div>
            );
        }
    }
    const renderOrignalPrice = (item) => {

        if (offer) {

            if (item != null) {
                let original_price = item.selling_price - (item.selling_price / 100 * discount)
                return original_price
            } else {

                let original_price = selling_price - (selling_price / 100 * discount)
                return original_price
            }

        } else {
            if (item != null) {
                return item.selling_price
            } else {
                return selling_price
            }
        }
    }

    const renderOverview = () => {
        if (product.description != null) {
            return (
                <Row>
                    <Col span={24} className='m-3'>
                        <Title level={4} style={{ fontSize: isMobileScreen ? 16 : 21 }}>OVERVIEW</Title>
                        <Divider />
                        {parse(product.description)}
                    </Col >
                </Row>
            );
        }
    }

    return (

        <main>
            <Head>
                <title>{meta_title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content={meta_description} />
                <meta property="og:title" content={og_title} />
                <meta property="og:description" content={og_description} />
                <meta property="og:url" content={og_url} />
                <meta property="twitter:title" content={twitter_title} />
                <meta property="twitter:description" content={twitter_description} />
                <meta property="twitter:summary" content={twitter_summary} />
                <meta property="twitter:url" content={twitter_url} />
            </Head>
            <div className='py-3' >
                <Row className='p-3 border rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        {/* <Breadcrumb.Item href={'/products/' + category.slug}>{category.name}</Breadcrumb.Item> */}
                        <Breadcrumb.Item>{name}</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <Row gutter={[20, 20]} className='py-3 ' >

                    <Col xs={24} md={12}>
                        <Carousel ref={ref} dots={true}>
                            {product.images != undefined && product.images.length > 0 ? product.images.map((v, k) => (
                                <Image className=' border' key={k} src={v.url} width={width} height={width} />
                            )) : null}
                        </Carousel>
                        <div className={classes.horizontal_slider}>
                            <div className={classes.slider_container}>
                                {product.images != undefined && product.images.length > 0 ? product.images.map((v, k) => (
                                    <div className={classes.item} key={k}  >
                                        <Image className=' border' onClick={() => goTo(k)} src={v.url} width={80} height={80} />
                                    </div>
                                )) : null}
                            </div>
                        </div>
                        {product.stock > 0 && <div className='border text-center p-2'>In Stock</div>}
                    </Col>
                    <Col xs={24} md={12} >
                        <Title level={2} style={{ fontSize: isMobileScreen ? 18 : 25 }} >{product.name}</Title>
                        <Text style={{ fontSize: isMobileScreen ? 12 : 17, fontWeight: '500' }}>{parse(product.short_description)}</Text><br />
                        {product.has_variant
                            ?
                            <>
                                <Radio.Group style={{ width: '100%' }} onChange={(e) => { setvariant(e.target.value) }}>
                                    {
                                        variants != undefined && variants.length > 0 ? variants.map(item => {
                                            return (
                                                <List direction="vertical" key={item.id} >
                                                    <List.Item style={{ display: 'flex', justifyContent: 'space-between' }} className="px-3 my-2 border">
                                                        <Radio value={item} >
                                                            <Space size='large'>
                                                                <span style={{ fontSize: 17, fontWeight: '500' }}>{item.attribute1_value}</span>
                                                                <span style={{ fontSize: 17, fontWeight: '500' }}>{item.attribute2_value}</span>
                                                                <span style={{ fontSize: 17, fontWeight: '500' }}>{item.attribute3_value}</span>
                                                            </Space>

                                                        </Radio>
                                                        <div className='my-2'>
                                                            <Title level={4} style={{ color: ' #000' }} >{currency} {renderOrignalPrice(item)}</Title>
                                                            {renderOffer()}
                                                        </div>
                                                    </List.Item>
                                                </List>
                                            )
                                        }) : null
                                    }
                                </Radio.Group><br />
                                <Row>
                                    <Col md={4} xs={8}>
                                        <div className={classes.anticon} style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', border: '2px solid black', borderRadius: '20px', height: '32px' }}>
                                            <p onClick={() => quantity > 1 && setQuantity(quantity - 1)}><MinusOutlined /></p>
                                            <p>{quantity}</p>
                                            <p onClick={() => setQuantity(quantity + 1)} ><PlusOutlined /></p>
                                        </div>
                                    </Col>
                                </Row>
                                <Button onClick={() => handleVariant()} className={isMobileScreen ? classes.btn_add_tocart : 'my-3'} type='primary' danger size='large'>Add To Cart</Button>
                            </>
                            :
                            <>
                                <div className='my-2'>
                                    <Title level={4} style={{ color: ' #000', fontSize: isMobileScreen ? 12 : 21 }} >{currency} {renderOrignalPrice()}</Title>
                                    {renderOffer()}
                                </div>
                                <Row>
                                    <Col md={4} xs={8}>
                                        <div className={classes.anticon} style={{ display: 'flex', justifyContent: 'space-between', padding: '0px 10px', border: '2px solid black', borderRadius: '20px', height: '32px' }}>
                                            <p onClick={() => quantity > 1 && setQuantity(quantity - 1)}><MinusOutlined /></p>
                                            <p>{quantity}</p>
                                            <p onClick={() => setQuantity(quantity + 1)} ><PlusOutlined /></p>
                                        </div>
                                    </Col>
                                </Row>
                                <Button onClick={() => handleAddToCart()} className={isMobileScreen ? classes.btn_add_tocart : 'my-3'} type='primary' danger size='large'>Add To Cart</Button><br />
                            </>
                        }
                    </Col>
                    {renderOverview()}
                </Row>
                <Divider />
                {related_products != undefined && related_products.length > 0 &&
                    <Row>
                        <Col span={24} >
                            <Title level={2}> Related Products</Title>
                            <ProductContent products={related_products} />
                        </Col>
                    </Row>

                }
            </div>
        </main >
    )
}

export default ProductDetail;

export const getServerSideProps = async (context) => {

    let data = {
        product: '',
        variants: [],
        id: 0
    };

    if (context != undefined && context.params != undefined) {

        const slug = context.params.slug.trim();

        const response = await ajaxService.get(`product/` + slug);

        if (response !== undefined && response !== null) {
            data = response;
        }
    }


    return {
        props: { data, key: data.product.id || 0 },
    }
}
