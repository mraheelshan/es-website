import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';

const { Title, Text } = Typography;

function AboutUs() {

    return (
        <main>
            <Head>
                <title>About Us</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 border rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>About Us</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='border mt-3 p-3' >
                    <Title>About Us</Title>
                    <Title level={3}>Adeel Fragrance - Unveiling the Essence of Luxury</Title>
                    <Text>
                        Welcome to Adeel Fragrance, where fragrance meets artistry, and the scent of tradition intertwines with the allure of the modern. Our online perfume boutique is a testament to our passion for perfumes and our commitment to sharing the magic of scents with you.
                    </Text>
                    <Title level={3}>Our Story</Title>
                    <Text>
                        At Adeel Fragrance, we believe that every scent has a story to tell. Our journey began when Mr. Adeel, the visionary behind our brand, embarked on a quest to discover the finest fragrances from the heart of the Arabian Peninsula. It was a journey that took him to remote villages, ancient markets, and the workshops of master perfumers.
                        Intrigued by the rich history and cultural significance of Arabian perfumery, Mr. Adeel sought to preserve and celebrate this tradition. He founded Adeel Fragrance with the goal of making these exquisite fragrances accessible to connoisseurs and enthusiasts worldwide.
                    </Text>
                    <Title level={3}>Our Philosophy</Title>
                    <Text>
                        At Adeel Fragrance, we are dedicated to offering you more than just perfumes; we offer an experience. Our philosophy is built on three core pillars:
                    </Text>
                    <Title level={4}>Authenticity:</Title>
                    <Text>
                        We are committed to sourcing and curating only the most authentic and high-quality Arabian perfumes. Each fragrance we offer is a piece of art crafted with precision and care, using time-honored techniques passed down through generations.
                    </Text>
                    <Title level={4}>Diversity:</Title>
                    <Text>
                        Our collection represents the diverse tapestry of Arabian perfumery. From the sultry, exotic scents of Oud to the delicate floral notes of Rose, we offer a wide range of fragrances to suit every taste and occasion.
                    </Text>
                    <Title level={4}>Customer-Centric:</Title>
                    <Text>
                        Your satisfaction is our priority. We strive to provide a seamless online shopping experience, from informative product descriptions to secure payment options and prompt delivery.
                    </Text>
                    <Title level={3}>Our Products</Title>
                    <Text>
                        At Adeel Fragrance, you will find a carefully curated selection of Arabian perfumes, including:
                    </Text>
                    <Title level={4}>Oud Perfumes:</Title>
                    <Text> Experience the mystique of Oud, a precious resin that embodies the essence of the Arabian desert.</Text>
                    <Title level={4}>Attar and Misk:</Title>
                    <Text> Discover the allure of traditional perfumes, handcrafted from the finest ingredients.</Text>
                    <Title level={4}>Bakhoor and Incense:</Title>
                    <Text> Elevate your space with the enchanting aromas of Bakhoor and Incense, a timeless Arabian tradition.</Text>
                    <Title level={4}>Modern Blends:</Title>
                    <Text> Explore contemporary fragrances that blend traditional elements with a modern twist.</Text>
                    <Title level={3}>Why Choose Adeel Fragrance</Title>
                    <Title level={4}>Quality Assurance:</Title>
                    <Text> We take pride in offering only authentic, high-quality products sourced from trusted artisans and perfumers.</Text>
                    <Title level={4}>Expert Guidance:</Title>
                    <Text> Our team is passionate about perfumery and is always ready to assist you in finding the perfect fragrance.</Text>
                    <Title level={4}>Secure Shopping:</Title>
                    <Text> Shop with confidence, knowing that your personal information is protected through secure payment methods.</Text>
                    <Title level={4}>Worldwide Delivery:</Title>
                    <Text> We ship our exquisite perfumes to customers around the globe, bringing the essence of Arabia to your doorstep.</Text>
                    <Title level={3}>Join Us on this Fragrant Journey</Title>
                    <Text>
                        Whether you are a seasoned connoisseur or just beginning to explore the world of Arabian perfumery, Adeel Fragrance welcomes you with open arms. Join us on this fragrant journey and experience the beauty, culture, and elegance of Arabian perfumery like never before.
                        Thank you for choosing Adeel Fragrance. We look forward to being your trusted partner in your olfactory adventures.
                    </Text>
                    <Text>
                        At Adeel Fragrance, we are passionate about the transformative power of fragrance. Our mission is to curate a captivating collection of perfumes that inspire and empower individuals to express their unique identity.
                    </Text>
                </div>
            </div>
        </main>
    )
}
export default AboutUs