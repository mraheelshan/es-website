import { useEffect, useState } from 'react';
import Head from 'next/head'
import Meta from 'antd/lib/card/Meta';
import { Typography, Row, Breadcrumb, Button } from 'antd';
import ajaxService from '../services/ajax-service';
import ReactHtmlParser from 'react-html-parser';
import Link from 'next/link';

function Blogs() {

    const [blogs, setBlogs] = useState([])
    const [ellipsis, setEllipsis] = useState(true);

    useEffect(() => {

        const getCommonData = async () => {
            const response = await ajaxService.get(`blogs`);

            if (response.authorize && response.valid) {
                setBlogs(response.payload)
            }
        }

        getCommonData();
    }, [])

    const getDate = (dateString) => {

        const parsedDate = new Date(Date.parse(dateString));

        const formattedDate = parsedDate.toLocaleString('en-US', {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
        });

        return (formattedDate);
    }

    return (
        <main>
            <Head>
                <title>Adeel Fragrance Blogs</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="keywords" content={`Adeel Fragrance Blogs`} />
            </Head>

            <div className='py-3' >
                <Row className='p-3 border rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Blogs</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                {
                    blogs.map((blog, index) => {
                        return (
                            <div className="card mb-4 shadow p-3 mt-3" key={index}>
                                <div className="row no-gutters">
                                    <div className="col-md-4">
                                        <img src={blog.image} className="card-img" alt="image description" />
                                    </div>
                                    <div className="col-md-8">
                                        <div className="card-body">
                                            <h5 className="card-title">{blog.title}</h5>
                                            <Typography.Paragraph ellipsis={
                                                ellipsis
                                                    ? {
                                                        rows: 2,
                                                        expandable: false,
                                                        symbol: 'more',
                                                    }
                                                    : false
                                            } >
                                                {ReactHtmlParser(blog.blog)}
                                            </Typography.Paragraph>
                                            <div className="d-flex flex-column mt-5">
                                                <a href={`/blog/${blog.slug}`} className="btn btn-primary">
                                                Continue reading
                                                </a>

                                            </div>
                                            <div className="text-end">
                                                <p className="fs-5"><i>{getDate(blog.created_at)}</i></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </main >
    )
}
export default Blogs