import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';
import ajaxService from '../../services/ajax-service';
import ReactHtmlParser from 'react-html-parser';
const { Title, Paragraph, Text } = Typography;

function Blog({ data }) {

    const getDate = (dateString) => {

        const parsedDate = new Date(Date.parse(dateString));

        const formattedDate = parsedDate.toLocaleString('en-US', {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
        });

        return (formattedDate);
    }

    const {
        id,
        title,
        status,
        image,
        blog,
        created_at,
        updated_at,
        meta_title,
        meta_description,
        slug,
    } = data;


    return (
        <main>
            <Head>
                <title>{meta_title}</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="meta_titles" content={`${meta_description}`} />
            </Head>
            <div className='py-3' >
                <Row className='p-3 border rounded' style={{ flex: 1, justifyContent: 'space-between' }}>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>{title}</Breadcrumb.Item>
                    </Breadcrumb>
                    <Text className='text-right'>
                        <em>Published on</em> <b>{getDate(created_at)}</b>
                    </Text>
                </Row>
                <div className='border mt-3 p-3' >
                    <Title>{title}</Title>
                    <Paragraph>
                        {ReactHtmlParser(blog)}
                    </Paragraph>
                </div>
            </div>
        </main>
    )
}
export default Blog

export const getServerSideProps = async (context) => {

    const slug = context.params.slug;

    let data = {
        id: 6,
        title: "",
        status: "",
        image: "",
        blog: "",
        created_at: new Date(),
        updated_at: "",
        meta_title: "",
        meta_description: "",
        slug: ""
    };

    const response = await ajaxService.get(`blogs/` + slug);

    if (response.valid) {
        data = { ...response.payload };
    }

    return {
        props: { data, key: slug }
    }
}