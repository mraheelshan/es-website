import React, { useEffect, useState } from 'react';
import '../styles/globals.css'
import Head from "next/head";
import { Provider } from 'react-redux';
import { createWrapper } from 'next-redux-wrapper'
import store from '../redux';
import Layout from '../components/layout'
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.css'
import { LayoutProvider } from '../providers/layout-provider'
import ajaxService from '../services/ajax-service';
import { useRouter } from 'next/router'
import ReactGA from 'react-ga';

function Loading() {
  const router = useRouter();

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const handleStart = (url) => (url !== router.asPath) && setLoading(true);
    const handleComplete = (url) => (url === router.asPath) && setLoading(false);

    router.events.on('routeChangeStart', handleStart)
    router.events.on('routeChangeComplete', handleComplete)
    router.events.on('routeChangeError', handleComplete)

    return () => {
      router.events.off('routeChangeStart', handleStart)
      router.events.off('routeChangeComplete', handleComplete)
      router.events.off('routeChangeError', handleComplete)
    }
  })

  return loading && (<div className="loading-container"><div className="lds-ripple"><div></div><div></div></div></div>);
}


const DefaultLayout = ({ children, isMobileScreen }) => {

  return (
    <div style={{ height: '100%', padding: '0px', paddingLeft: (isMobileScreen ? '10px' : '80px'), paddingRight: (isMobileScreen ? '10px' : '80px'), backgroundColor: '#fff' }}>
      {children}
    </div>
  );
};


function MyApp({ Component, pageProps }) {
  const [isMobileScreen, setIsMobileScreen] = useState(false)
  const [ticker, setTicker] = useState('')

  useEffect(() => {
    // Initialize Google Analytics with your tracking ID
    ReactGA.initialize('G-XV9GX8FHH8'); // Replace with your tracking ID

    // Send a pageview event to Google Analytics on each route change
    ReactGA.pageview(window.location.pathname + window.location.search);
  }, []);  

  useEffect(() => {

    const getCommonData = async () => {
      const response1 = await ajaxService.get(`ticker/get`);
      setTicker(response1.payload)
    }

    getCommonData();
  }, [])

  useEffect(() => {
    const handleWidth = () => {
      const width = document.body.clientWidth
      return (width <= 768)
    }

    setIsMobileScreen(handleWidth())
  })

  const PageLayout = Component.Layout === null ? React.Fragment : Component.Layout || DefaultLayout;

  return (
    <>
      <Provider store={store} >
        <LayoutProvider>
          <Layout ticker={ticker}>
            <Head>
              <meta name="viewport" content="width=device-width, initial-scale=1" />
              <script
                type="application/ld+json"
                dangerouslySetInnerHTML={{
                  __html: JSON.stringify({
                    "@context": "https://schema.org",
                    "@type": "WebSite",
                    "url": "https://adeelfragrance.com",
                    "name": "Adeel Fragrance",
                    "description": "Beauty, cosmetic & personal care"
                  })
                }}
              />
            </Head>
            <Loading />

            {
              Component.Layout === null &&
              <div>
                <Component {...pageProps} isMobileScreen={isMobileScreen} />
              </div>
            }

            {
              Component.Layout !== null &&
              <PageLayout isMobileScreen={isMobileScreen} >
                <Component {...pageProps} isMobileScreen={isMobileScreen} />
              </PageLayout>
            }

          </Layout>
        </LayoutProvider>
      </Provider>
    </>
  )
}

const makeStore = () => store;
const wrapper = createWrapper(makeStore)
export default wrapper.withRedux(MyApp);

