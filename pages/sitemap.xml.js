// pages/sitemap.xml.js

import ajaxService from "../services/ajax-service";

const URL = "https://adeelfragrance.com";

function generateSiteMap(products) {
    return `<?xml version="1.0" encoding="UTF-8"?>
     <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
       <!-- Add the static URLs manually -->
       <url>
         <loc>${URL}</loc>
       </url>
       <url>
         <loc>${URL}/contact</loc>
       </url>
        <url>
         <loc>${URL}/about-us</loc>
       </url>
       ${products
            .map(({ slug }) => {
                return `
             <url>
                 <loc>${`${URL}/product/${slug}`}</loc>
             </url>
           `;
            })
            .join("")}
     </urlset>
   `;
}



export async function getServerSideProps({ res }) {

    const response = await ajaxService.get(`search/all`);

    // Generate the XML sitemap with the blog data
    const sitemap = generateSiteMap(response.products);

    res.setHeader("Content-Type", "text/xml");
    // Send the XML to the browser
    res.write(sitemap);
    res.end();

    return {
        props: {},
    };
}

export default function SiteMap() { }
