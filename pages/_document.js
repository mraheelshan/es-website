import { Html, Head, Main, NextScript } from 'next/document'
import { useEffect } from 'react';


export default function Document() {
    
    return (
        <Html>
            <Head>
                <a rel="icon" href="/favicon.ico" />
            </Head>
            <body>
                <Main />
                <NextScript />
            </body>
        </Html>
    )
}