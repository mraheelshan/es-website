import Head from 'next/head'
import { Typography, Row, Breadcrumb } from 'antd';

const { Title, Text } = Typography;

function TermsAndConditions() {
    return (
        <main>
            <Head>
                <title>Terms and Conditions</title>
            </Head>
            <div className='py-3' >
                <Row className='p-3 bg-white rounded'>
                    <Breadcrumb separator=">">
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>Terms and Conditions</Breadcrumb.Item>
                    </Breadcrumb>
                </Row>
                <div className='bg-white mt-3 p-3' >
                    <Title>Terms and Conditions</Title>
                    <Title level={2}>A. INTRODUCTION</Title>
                    <Text>
                        1. www.adeelfragrance.com (the "Website") is owned by Adeel Iqbal("AdeelPerfumes/we/us"), with delivery services provided by Australia Post. If you have any questions regarding the Website or these terms and conditions, or in the unlikely event that you have any complaints about any products purchased by you from the Website or through any Applications (as defined below), you can contact us at adeel173@hotmail.com
                    </Text>
                    <br /><br />
                    <Text>
                        In case of any disputes, the law applicable would be based on the Australian Governing Law.
                    </Text>
                    <br /><br />
                    <Text>
                        2. If you select a shipping destination in the Australia , please go through the Terms and Conditions thoroughly which shall apply to your purchase and use of the Website.
                    </Text>
                    <Title level={2}>B. USE OF WEBSITE</Title>

                    <Text>1.    These terms and conditions and any other policies referred to in these terms and conditions (including any policies or documents to which a link is provided from these terms and conditions) (together the "Terms") apply to your use of and access to the Website and any other website or application permitting you to place an order with Adeel Fragrance for any products and services (such websites and applications being the “Applications” for the purpose of these Terms) including all orders submitted by you for any products or services made available by us for purchase over the Website and/or Applications. As the context requires, references to “Website” in these Terms shall also include Applications as applicable. By accessing this Website and/or the Applications you agree to these Terms and Conditions, we therefore advise you to read these Terms carefully and to save or print a copy of these Terms and Conditions for FUTURE reference. If you do not agree to these Terms, you must cease using and accessing this Website and all Applications immediately. The Terms may be changed and updated from time to time and any changes will be effective from the publication of the new terms on the Website or the relevant Application. Please note that all options available on our Website may not be available on any Application or the Website accessed using a mobile device.
                    </Text>
                    <br /><br />
                    <Text>
                        2.    Please note that these Terms do not affect your statutory rights as a consumer.
                    </Text><br /><br /><Text>
                        3. You agree that the information you provide when you REGISTER on the Website is not misleading, and is true and accurate in all respects and you will notify our customer service team of any changes to that information.
                    </Text><br /><br /><Text>
                        4. We may change, withdraw, or suspend access to the Website (in whole or part and permanently or temporarily) with or without notice and with no LIABILITY to you.

                    </Text><br /><br /><Text>
                        5. Adeel Fragrance may deny you access to the Website at any time in its sole discretion. Examples of when we may so deny you access include but is not limited to where we believe that your use of the Website is in violation of any of these Terms, any law or the rights of any third party or was not respectful to
                    </Text><br /><br /><Text>

                        6. Adeel Fragrance will not be responsible, or liable to you or any third party, for the content or accuracy of any materials posted by you or any other user of the Website and you hereby agree to be responsible to Adeel fragrance for and indemnify Adeel fragrance and keep Adeel fragrance indemnified against all COSTS, damages, expenses, losses and liabilities incurred and/or suffered by Adeel fragrance as a result of any claim in respect of your use of the Website.
                    </Text><br /><br /><Text>
                        7. Adeel fragrance has the right to remove any material or posting you make on the Website in Adeel fragrance sole discretion.
                    </Text>
                    <br /><br />

                    <Title level={2}>C. PURCHASE OF PRODUCTS</Title>
                    <Title level={3}>1. ACCEPTANCE OF ORDERS</Title>

                    <Text>
                        1.1  All information on the Website is an invitation to treat only and is not an offer or unilateral CONTRACT. You agree that your order is an offer to purchase the products listed in your order ("Products") from us on the Terms. All orders submitted by you are subject to acceptance by us. We may choose not to accept your order in our discretion for any reason whatsoever without LIABILITY to you. Examples of when we may not accept your order are as follows:
                    </Text><br /><br />
                    <ul>
                        <li>    (a) If products are shown on the Website but are not available or are incorrectly priced or otherwise incorrectly described;</li>
                        <li>   (b) If we are unable to obtain authorization of your payment;</li>
                        <li> (c) If you order multiple quantities of an individual Product where such Products are to be shipped to different customer or delivery address;</li>
                        <li>   (d) If shipping restrictions may apply to a Product; or</li>
                        <li>  (e) If the delivery address you give is the address of an entity or individual providing freight forwarding services.</li>
                    </ul>
                    <Text>
                        1.2 After submitting an order to us, we will send you an order acknowledgement email with your order number and details of the Products you have ordered from us and details of any delivery services (the "Delivery Services") you have ordered from Adeel Fragrance . Please note that this email is an acknowledgement that we have received your order and is not an acceptance of your order. You will be receiving a shipment confirmation email from Adeel Fragrance which is an acknowledgement that we have accepted your order.
                    </Text><br /><br /><Text>
                        Acceptance of your order and the formation of (a) an order of sale of the Products between us and you and (b) an order for Delivery Services between us and you. In the event Adeel Fragrance partnered with shipping service company to dispatch the products to end customer and in the meantime, we have sent you an email confirming that the products have been dispatched through Courier company.Further information is available at Shipping Destinations, Weight, Costs and Delivery Times.
                    </Text><br /><br /><Text>
                        1.3 When placing an order for the first time, you may be required to or may be offered the option to REGISTER with us and complete certain required fields on an order form. We may provide you with and/or ask you to use identifications and passwords and other means for you to be able to access certain areas of the Website, such as the My Account section of the Website. You shall comply with all security directions and/or recommendations given by us and inform us immediately if you become aware of or suspect any unauthorised use of the Secure Access or if the Secure Access becomes available to an unauthorised party. Without prejudice to our other rights and remedies, we may suspend your access to the Website without LIABILITY to you, if in our reasonable opinion, such action is necessary for safeguarding the Website.
                    </Text><br /><br /><Text>
                        1.4 Before you submit your order, you will be given the opportunity to review your selection, check the total price of your order and correct any input errors.
                    </Text><br /><br /><Text>
                        1.5 We do not accept orders where the corresponding delivery address you give is that of an inappropriate. In the event that we do accept any order and we subsequently become aware that the delivery address is inappropriate for such order is that of no services, we shall be entitled to cancel such order upon notice to you by email or telephone.
                    </Text>

                    <Title level={3}>2. PRICES</Title>
                    <Text>
                        2.1 All prices of Products on the Website are the price for the Products only. Find out more about Shipping Destinations, Weight, Costs and Delivery Times.
                    </Text><br /><br /><Text>
                        2.2 Adeel Fragrance may vary the prices of Products listed on the Website at any time and without any notice but such changes will not apply to Products in respect of which you have been sent a parcel.
                    </Text><br /><br /><Text>
                        2.4 Free Shipping Over $50
                    </Text><br /><br /><Text>
                        Get free shipping when you spend over $50 and receive your order within 2-3 working days. You can check out our shipping policies page for more information and details.
                    </Text><br /><br />

                    <Title level={3}>3. PAYMENT TERMS</Title>
                    <Text>
                        3.1 The total cost of your order will be the purchase price for the Products (which you pay to us) plus any delivery charge. Find out more about Shipping Destinations, Weight, Costs and Delivery Times.
                    </Text><br /><br /><Text>
                        3.2 Please note that we accept payment in the payment CURRENCY specified for the country of your selected shipping destination in our Payment section.
                    </Text><br /><br /><Text>
                        3.3 You confirm that the credit/debit card or payment method that is being used is yours and that all details you provide to us in respect thereof including, without limitation, name and address details are complete, correct and accurate . You further confirm that the credit/debit card is valid and the inputted payment details are correct. All credit/debit cardholders and payment ACCOUNT holders are subject to validation checks and authorization by the card issuer or payment method provider. If the issuer of your card or payment method refuses to authorize payment we will not accept your order and we will not be liable for any delay or non-delivery and we are not obliged to inform you of the reason for the refusal.
                    </Text><br /><br /><Text>
                        3.4 We are not responsible for any charges or other amounts which may be applied by your card issuer or bank or payment method provider as a result of our processing of your credit/debit card payment or other method of payment in accordance with your order.
                    </Text><br /><br /><Text>
                        3.5 If your credit/debit card or payment method is not denominated in the currency of your PURCHASE INDICATED on the Website, the final price may be charged in the currency of your card or account. Such final price is calculated and charged by your card issuer or bank or payment method provider and therefore we shall not be responsible for any cost, expense, charge or other liability which may be incurred or suffered by you as a result of your card issuer or payment method provider charging you in a different currency other than the currency of your purchase as displayed on the Website.
                    </Text>

                    <Title level={3}>4. INVOICING</Title>
                    <Text>
                        4.1 Where we elect, or are required by applicable law, to issue or make available an invoice, we reserve the right to issue or make available electronic invoices and you agree to such form of INVOICING.
                    </Text><br /><br />

                    <Title level={3}>5. DELIVERY AND RISK</Title>
                    <Text>
                        5.1 We currently deliver the products only in Australia and New Zealand.
                    </Text><br /><br /><Text>

                        5.2 When you have selected your preferred delivery method from those offered for your selected shipping destination and provided your order has been accepted by us, your order shall be processed by us to dispatch your order in accordance with the estimated delivery times set out at Shipping Destinations, Weight, Costs and Delivery Times. Please refer shipping restrictions before placing an order. Orders received after any specified “cut off” or “last order” time or received on a day which is not a working day (that is any day on which the banks are open for business in Australia which is not a Saturday or a Sunday, will be processed on the next working day). Estimated delivery times will be CALCULATED from the date on which the order is processed.
                    </Text><br /><br /><Text>


                        5.3 Delivery information such as Customer Name, Location Address and Contact Number will be stored for delivery purposes. Other personal identifiable information will not be stored.
                    </Text>

                    <Title level={3}>6. CANCELLATION AND RETURNS</Title>
                    <Text>
                        6.1 Should you wish to cancel or return any Products, you may only do so in accordance with our Returns & Cancellation policy. This Returns & Cancellation Policy does not affect your statutory rights as a consumer distance selling legislation or e-commerce regulations in the territory to which the product is shipped (“Regulations”).
                    </Text><br /><br /><Text>
                        6.2 Where you return a Product under the Regulations we will issue you with a full refund but you will need to return the Product at your own cost (if you have already received the Product), unless otherwise specified in our Returns & Cancellation policy.
                    </Text><br /><br />
                    <Title level={3}>7. AGE REQUIREMENTS</Title>

                    <Text>
                        7.1 If you order a Product to which a minimum age requirement applies, by ordering that Product you confirm that you are of the required age. If we reasonably believe that you are not legally entitled to order a Product, we reserve the right to cancel your order.
                    </Text><br /><br />

                </div>
            </div>
        </main>
    )
}
export default TermsAndConditions